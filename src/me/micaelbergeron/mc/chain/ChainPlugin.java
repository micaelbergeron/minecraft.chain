package me.micaelbergeron.mc.chain;

import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created with IntelliJ IDEA.
 * User: micael
 * Date: 13-09-04
 * Time: 19:24
 * To change this template use File | Settings | File Templates.
 */
public class ChainPlugin extends JavaPlugin {

    @Override
    public void onEnable() {
        super.onEnable();

        this.getServer().broadcastMessage("HELLO WORLD!");
    }

    @Override
    public void onDisable() {
        super.onDisable();

        this.getServer().broadcastMessage("GOODBYE WORLD!");
    }
}
